
import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, StyleSheet, Text } from 'react-native';

const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      editable
      maxLength={40}
    />
  );
}

const UselessTextInputMultiline = () => {
  const [value, onChangeText] = React.useState();

  // If you type something in the text box that is a color, the background will change to that
  // color.
  return (
    <View
      style={{
        padding: 10,
        marginTop:30,
        width:335,
        height:300,
        borderRadius:20,
        left:30,
        borderWidth:2,
        paddingHorizontal:40,
        paddingVertical:8,
        marginBottom:20
        

        
      }}>
      <UselessTextInput
        multiline
        numberOfLines={4}
        placeholder="Enter your text" 

        onChangeText={text => onChangeText(text)}
        value={value}
      />
      <View>
      <TouchableOpacity style={styles.button}
            onPress={() => {}}>
              <Text style={styles.start}> Submit   </Text>
          </TouchableOpacity>

      </View>
         
      
    </View>
  );
}
const styles = StyleSheet.create({
 
  button: {
    backgroundColor: "#FAD9D2",
    padding: 5,
    marginTop:250,
    width:150,
    height:50,
    borderRadius:27,
    justifyContent:'center',
    left:30
    //TODO

  },
  start:{
    fontSize:24,
    fontWeight:'normal',
    color:'#000000',
    textAlign:'center'

  },
 
  
});
export default UselessTextInputMultiline;
