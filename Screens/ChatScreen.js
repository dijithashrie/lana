import React, { Component } from 'react';
import { View, Text,TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { style, styles } from './index';  

export default class ChatScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View>
         <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity 
            style={{marginTop:20}}
            onPress={this.props.navigation.openDrawer}>               
                <Icon name="ios-menu" size={40} color="#ff4757" />
            </TouchableOpacity>
          </View>
        <Text style={[styles.textstyle, {marginTop:100}]}> Create an Avatar for Lana</Text>
        <Text> ☺️😂😀😡🙁😌😍</Text>

      </View>
    );
  }
}


