import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';  
import { style, styles } from './index';

export default class MyExcerscisesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity 
            style={{marginTop:20}}
            onPress={this.props.navigation.openDrawer}>               
                <Icon name="ios-menu" size={40} color="#ff4757" />
            </TouchableOpacity>
          </View>

        <Text style={styles.textstyleMyJournal}> Morning  Jane!! lets have a look at today's target, shall we.</Text>
        <View>
        <TouchableOpacity style={styles.buttonMyJournal}
            onPress={() => {}}>
              <Text style={[styles.start, {textAlign:'center'}]}> Go to a Jog </Text>
              <Text style={{ fontSize: 16, textAlign:'center', marginTop:5}}> Excercise a bit to relax your mind and start fresh </Text>

        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonMyJournal, {backgroundColor:'#E2FFCC'}]}
            onPress={() => {}}>
              <Text style={[styles.start, {textAlign:'center'}]}> Meditate </Text>
              <Text style={{ fontSize: 16, textAlign:'center', marginTop:5}}> Meditate for 10 min in the morning before work  </Text>

        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonMyJournal, {backgroundColor:'#8DD1D4'}]}
            onPress={() => {}}>
              <Text style={[styles.start, {textAlign:'center'}]}> Journal Entry  </Text>
              <Text style={{ fontSize: 16, textAlign:'center', marginTop:5}}> Write about the most difficult experience you had and how you overcame it.  </Text>

        </TouchableOpacity>

        </View>
       
      </View>
    );
  }
}
;