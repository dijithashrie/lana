import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';


export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <LinearGradient
          // Background Linear Gradient
          colors={['rgba(2,181,227,100)', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 320,
            borderTopLeftRadius:30,
            borderTopRightRadius:30,
            paddingVertical:30,
            paddingHorizontal:20
            
          }}
        />
        <Text style={[styles.textstyle, {marginTop:90}]}> Hi Jane !!</Text>
        <Text style={styles.textstyle}> I'm  Lana</Text>
        <Text style={styles.textstyle}> It's nice to meet you!</Text>
        <View style={styles.footer}>
        <TouchableOpacity  style={styles.button}
        onPress={() => {this.props.navigation.navigate("Personalize")}}>

          <Text style={styles.start}> Let's get started !! </Text>
        </TouchableOpacity>
        

        </View>
      

        
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#fff',
    
  },
  textstyle:{
    fontSize:24,
    fontWeight:'normal',
    textAlign:'center',
    fontSize:24,
    marginTop:10
  },
  start:{
    fontSize:24,
    fontWeight:'normal',
    color:'#000000',
    textAlign:'center'



  },
 
  footer:{

    flex: 1
  },
  button: {
    backgroundColor: "#247BAB",
    padding: 10,
    marginTop:150,
    width:300,
    height:60,
    borderRadius:27,
    justifyContent:'center',
    //TODO
    left:50

  },
    
});