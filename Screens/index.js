import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
        
      },
      signinText:{
    
        fontStyle:"normal",
        fontWeight:"normal",
        fontSize:26,
        alignItems:'center',
        textAlign:"center",
        color:'#000000',
        
        
        
      },
      header:{
        flex:2,
        justifyContent:'flex-end',
           
      },
      footer:{
        flex:1,
    
        
      },
      insta:{
        fontStyle:"normal",
        fontSize:18,
        textDecorationLine: 'underline',
        textAlign:'center',
    
    
      },
      button:{
        backgroundColor:'#ffffff',
        borderRadius:25,
        marginTop:20,
        marginBottom:30,
        padding:10
    
      },
      logo:{
        alignItems:'center',
        marginLeft:50,
        marginRight:50,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        borderBottomLeftRadius:30,
        borderBottomRightRadius:30,
        paddingVertical:30,
        paddingHorizontal:20,
        marginBottom:20
    
      },

      /****************** Prefer*/

     /* container: {
        backgroundColor: '#FFFFFF',
      },*/


      buttonPrefer: {
        backgroundColor: "#FAD9D2",
        padding: 10,
        marginTop:30,
        width:335,
        height:160,
        borderRadius:27,
        justifyContent: 'center',
    
      
       
    
      },
      start:{
        fontSize:25,
        fontWeight:'normal',
        color:'#000000',
        textAlign:'center',
        justifyContent:'center'
         },


         /**********  Avail*/
        
          availText:{
              fontSize:28,
              fontWeight:'normal',
              color:'#000000',
              textAlign:'center',
              justifyContent:'center',
              color:'#000000',
              marginTop:10
          },
          rectangle: {
              height: 32,
              width: 40,
              borderRadius:14,
              marginTop:30
              
              
            },
          inside:{
              fontStyle:'normal',
              fontWeight:'normal',
              textAlign:'center',
             
              fontSize:22,
              color:'#000000',
             
          
            },
          buttonAvail: {
              backgroundColor: "#FFFFFF",
              padding: 20,
              marginTop:30,
              width:200,
              height:60,
              borderRadius:27,
              justifyContent: 'center',
              alignItems: 'center',
              left:80
             
              
            
              //TODO
          
            },
         /* start:{
              fontSize:20,
              fontWeight:'normal',
              color:'#000000',
              textAlign:'center',
              justifyContent:'center'
          
          
            },*/
      
            buttonTime: {
              backgroundColor: "#4EB151",
              paddingVertical: 11,
              paddingHorizontal: 17,
              borderRadius: 10,
              marginVertical: 50,
              marginTop:100,
              width:200,
              height:60,
              borderRadius:27,
              left:80
            },
            buttonText: {
              color: "#FFFFFF",
              fontSize: 20,
              textAlign:'center'
            },

            /******** chat */

           /* container: {
                flex: 2,
                backgroundColor: '#fff',
                
              },*/
              textstyle:{
                  fontSize:24,
                  fontWeight:'normal',
                  textAlign:'center',
                  marginTop:10
                },


                /******* My journal */

              /*  container: {
                    flex: 2,
                    backgroundColor: '#fff',
                    
                  },*/
                  textstyleMyJournal:{
                      fontSize:24,
                      fontWeight:'normal',
                      textAlign:'center',
                      marginTop:20,
                      marginBottom:20,
              
                       },
                    buttonMyJournal: {
                      padding: 10,
                      marginTop:30,
                      width:335,
                      height:130,
                      borderRadius:20,
                      left:30,
                      borderWidth:2,
                      paddingHorizontal:40,
                      paddingVertical:8,
                      marginBottom:20
                      //TODO
                  
                    },
                  /*  start:{
                      fontSize:24,
                      fontWeight:'normal',
                      color:'#000000',
                  
                    },*/
                    rectangleMyJournal: {
                      height: 70,
                      width: 70,
                      borderRadius:14,
                      marginTop:30
                      
                      
                    },
                  insideMyJournal:{
                    
                     
                      fontSize:60,
                      justifyContent:'center',
                      color:'#000000',
                     
                  
                    },

                    /****** calendar */

                  /*  container: {
                        flex: 2,
                        backgroundColor: '#fff',
                        
                      },*/
                    /*  textstyle:{
                          fontSize:24,
                          fontWeight:'normal',
                          textAlign:'center',
                          marginTop:20,
                          marginBottom:20,
                     },*/
                     /*   button: {
                          padding: 10,
                          marginTop:30,
                          width:335,
                          height:130,
                          borderRadius:20,
                          left:30,
                          borderWidth:2,
                          paddingHorizontal:40,
                          paddingVertical:8
                          //TODO
                      
                        },*/
                       /* start:{
                          fontSize:24,
                          fontWeight:'normal',
                          color:'#000000',
                      
                        },*/

                        /****** Excercise */

                      /*  container: {
                            flex: 2,
                            backgroundColor: '#fff',
                            
                          },*/
                         /* textstyle:{
                            fontSize:24,
                            fontWeight:'normal',
                            textAlign:'center',
                            marginTop:20,
                            marginBottom:10
                          },*/
                        /*  button: {
                            backgroundColor: "#FAD9D2",
                            padding: 10,
                            marginTop:30,
                            width:335,
                            height:130,
                            borderRadius:27,
                            left:30
                            //TODO
                        
                          },*/
                        /*  start:{
                            fontSize:24,
                            fontWeight:'normal',
                            color:'#000000',
                        
                          },*/

                          /*** facebook */

                          containerfacebook: {
                            flex: 4,
                            backgroundColor: '#fff',
                            
                          },
                          headerFacebook:{
                            justifyContent:'flex-end',
                               
                          },
                          footerFacebook:{
                            flex:2,
                        
                            
                          },
                          rectangleFacebook: {
                           // height: 67,
                            //TODO:
                            backgroundColor: "#3B5998",
                            padding: 30,
                            height: 67,
                            alignItems: "center",
                            justifyContent: 'center',
                            
                        
                            //  position: 'absolute', 
                            
                          },
                          login:{
                            fontSize:24,
                            fontWeight:'normal',
                            color:'#FFFFFF',
                            fontSize:24,
                            justifyContent:'center',
                            textAlign:'center',
                            alignSelf:'center'
                            
                        
                        
                          },
                          logoFacebook:{
                            height:95,
                            width:95,
                          },
                          tstyle:{
                            fontStyle:'normal',
                            fontWeight:'normal',
                            textAlign:'center',
                            fontSize:18
                          },
                          bstyle:{
                            fontSize:14,
                            fontWeight:'normal',
                            textAlign:'center',
                            color:'#425FF9'
                          },
                          save:{
                            color:'#FFFFFF',
                            fontSize:18,
                            textAlign:'center',
                            marginTop:5
                          }
      

})