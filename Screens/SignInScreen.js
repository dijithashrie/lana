import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import {style,styles} from './index';

export default class SignInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image 
          style={styles.logo}

          source={require('../Images/lana.png')}
          />

        </View>

        <View style={styles.footer}>
        <LinearGradient
          // Background Linear Gradient
          colors={['rgba(2,181,227,100)', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 320,
            borderTopLeftRadius:30,
            borderTopRightRadius:30,
            paddingVertical:30,
            paddingHorizontal:20
            
          }}
        />
        <Text style={styles.signinText}> Sign In</Text>
        <View style={styles.button}>
        <TouchableOpacity onPress={() => {this.props.navigation.navigate("Preffer")}}>
          <Text style={styles.insta}> Continue With Instagram </Text>
        </TouchableOpacity>
        </View>
        <View style={[styles.button, {marginTop:5}]}>
        <TouchableOpacity onPress={() => {this.props.navigation.navigate("Facebook")}}>
       


          <Text style={styles.insta}> Continue With Facebook </Text>
        </TouchableOpacity>
        </View>


        </View>

      </View>
    );
  }
}


