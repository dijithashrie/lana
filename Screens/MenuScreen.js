import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Button, Image, ScrollView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import Icon from 'react-native-vector-icons/Ionicons';  




export default class MenuScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
 

  render() {
    return (
      <ScrollView style={styles.container}>
         {/* <LinearGradient
          // Background Linear Gradient
          colors={['rgba(2,181,227,100)', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 320,
            borderTopLeftRadius:10,
            borderTopRightRadius:10,
            paddingVertical:30,
            paddingHorizontal:20
            
          }}
        /> */}
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity 
            style={{marginTop:20}}
            onPress={this.props.navigation.openDrawer}>               
                <Icon name="ios-menu" size={40} color="#ff4757" />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={styles.textstyle}> Fun Fact </Text>
            <TouchableOpacity style={[styles.button, {backgroundColor:'#E2EEFF'}]}
            onPress={() => {}}>
              <Text style={[styles.start, {color:'#247BAB'}]}> Did you know that Mt. Augustus is twice the size of Uluru? </Text>
           </TouchableOpacity>
           <Text style={styles.textstyle}> Upcoming Appointmemnts </Text>
           {/* <TouchableOpacity style={[styles.button, {backgroundColor:'#E2EEFF'}]}
            onPress={() => {}}>
               <Image 
          style={{width:50, height:500,flex:4, left:250, alignItems: 'center'}}
          source={require('../Images/fb.png')}
          />
              <Text style={[styles.start, {color:'#247BAB'}]}> Sptember 21 </Text>
              <Text style={[styles.start, {color:'#247BAB'}]}> Friday  - 13:00 </Text>
              <Text style={[styles.start, {color:'#247BAB'}]}> Sydney Road  </Text>
             
           </TouchableOpacity> */}
           <TouchableOpacity onPress={() => {}} style={styles.button}>
          <View style={styles.btnContainer}>
            <Image source={require('../Images/fb.png')} style={styles.icon} />
            <Text style={styles.btnText}> Sptember 21 Friday  - 13:00      Sydney Road</Text>
          </View>
        </TouchableOpacity>
           
           <Text style={styles.textstyle}> Tasks for the day </Text>
        <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around'}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#B1E3E6'}]} onPress={() => {}}>
        <Image 
          style={{width:50, height:50,justifyContent:'center',  alignItems:'center'}}
          source={require('../Images/fb.png')}
          />
          <Text style={styles.inside}> Jogging  </Text>        
              
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#E2FFCC'}]} onPress={() => {}}>
        <Image 
          style={{width:50, height:50,justifyContent:'center',  alignItems:'center'}}
          source={require('../Images/fb.png')}
          />
          <Text style={styles.inside}> Meditate   </Text>        
              
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#B1E3E6'}]} onPress={() => {}}>
        <Image 
          style={{width:50, height:50,justifyContent:'center',  alignItems:'center'}}
          source={require('../Images/fb.png')}
          />
          <Text style={styles.inside}> Daily Entry   </Text>        
              
        </TouchableOpacity>
        </View>



           
          </View>
          <TouchableOpacity style={styles.button}
            onPress={() => {this.props.navigation.navigate('Chat')}}>
              <Text style={styles.start}> Let's chat a little   </Text>
          </TouchableOpacity>

      
     
          
        
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#fff',
    
  },
  textstyle:{
    fontSize:24,
    fontWeight:'normal',
    textAlign:'center',
    fontSize:24,
    marginTop:10
  },
  button: {
    backgroundColor: "#FAD9D2",
    padding: 10,
    marginTop:30,
    width:335,
    height:130,
    borderRadius:27,
    justifyContent:'center',
    left:30
    //TODO

  },
  start:{
    fontSize:24,
    fontWeight:'normal',
    color:'#000000',
    textAlign:'center'

  },
  rectangle: {
    height: 108,
    width: 89,
    borderRadius:14,
    marginTop:30
    
    
  },
  inside:{
    fontStyle:'normal',
    fontWeight:'normal',
    
    fontSize:14,
    color:'#000000',
   

  },
  btnContainer: {
    backgroundColor: '#1d2aba',
    paddingHorizontal: 70,
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10
  },
  button: {
    borderRadius: 5
  },
  icon: {
    width: 25,
    height: 25,
    left: 2, // Keep some space between your left border and Image
  },
  btnText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white'
  }
});