import React, { Component } from 'react';
import { View, Text , TouchableOpacity, StyleSheet, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';  
import { style, styles } from './index';


export default class MyJournalScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <ScrollView>
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity 
            style={{marginTop:20}}
            onPress={this.props.navigation.openDrawer}>               
                <Icon name="ios-menu" size={40} color="#ff4757" />
            </TouchableOpacity>
          </View>
          <Text style={styles.textstyleMyJournal}> July 2020 </Text>

     
        <TouchableOpacity style={styles.buttonMyJournal}
            onPress={() => {}}>
              <Text style={[styles.start, {textAlign:'center'}]}> Quote of the day  </Text>
              <Text style={[styles.start, {textAlign:'center'},{fontSize:20}, {color:'#247BAB'} ]}> If pinocchio says " My Nose Will Grow Now", it would cause a paradox. </Text>


        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonMyJournal,{backgroundColor:'#FAD9D2'}, {height:80}]}
            onPress={() => {}}>
              <Text style={[styles.start, {textAlign:'center'}]}> How do you feel today?  </Text>


        </TouchableOpacity>
         <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around',}} >
        <TouchableOpacity onPress={() => {}}>
          <Text style={styles.insideMyJournal}> 😂 </Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => {}}>
          <Text style={styles.insideMyJournal}> 😀 </Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => {}}>
          <Text style={styles.insideMyJournal}> 😡 </Text>
          
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => {}}>
          <Text style={styles.insideMyJournal}> 😌 </Text>
          
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => {}}>
          <Text style={styles.insideMyJournal}> 😍 </Text>
          
        </TouchableOpacity>
       


        </View>


        <TouchableOpacity style={styles.buttonMyJournal}
            onPress={() => {}}>
              <Text style={[styles.start, {textAlign:'center'}]}> Food for taught : </Text>
              <Text style={[styles.start, {textAlign:'center'},{fontSize:23}, {color:'#247BAB'} ]}> Reflect upon what you overcame this week. </Text>


        </TouchableOpacity> 
      </ScrollView>
    );
  }
}
;