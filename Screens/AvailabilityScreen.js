import React, { Component , useState} from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import TimePicker from "react-native-24h-timepicker";
import { style, styles } from './index';

export default class AvailabilityScreen extends Component {
  constructor() {
    super();
    this.state = {
      time: ""
    };
  }
  onCancel() {
    this.TimePicker.close();
  }
 
  onConfirm(hour, minute) {
    this.setState({ time: `${hour}:${minute}` });
    this.TimePicker.close();
  }

  render() {
    
    
    return (
      
      <ScrollView>
        <Text style={styles.availText}> What's your availability?</Text>
        <Text  style={[styles.availText, {color:'#428BF9'}, {fontSize:16}]}> Choose a time that best suits you for a catchup everyday. </Text>
        <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around',}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FDECCE'}]} onPress={() => {}}>
          <Text style={styles.inside}> M </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#C0E9FF'}]} onPress={() => {}}>
          <Text style={styles.inside}> T </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#8DD1D4'}]} onPress={() => {}}>
          <Text style={styles.inside}> W </Text>
          
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FDECCE'}]} onPress={() => {}}>
          <Text style={styles.inside}> T </Text>
          
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#E2FFCC'}]} onPress={() => {}}>
          <Text style={styles.inside}> F </Text>
          
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FCBBC1'}]} onPress={() => {}}>
          <Text style={styles.inside}> S </Text>
          
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#C1DAFF'}]} onPress={() => {}}>
          <Text style={styles.inside}> S </Text>
          
        </TouchableOpacity>


        </View>

        






        <View>
        <TouchableOpacity
          onPress={() => this.TimePicker.open()}
          style={styles.buttonTime}
        >
          <Text style={styles.buttonText}>Pick a time </Text>
        </TouchableOpacity>
        <Text style={styles.text}>{this.state.time}</Text>
        <TimePicker
          ref={ref => {
            this.TimePicker = ref;
          }}
          onCancel={() => this.onCancel()}
          onConfirm={(hour, minute) => this.onConfirm(hour, minute)}
        />


        </View> 
        
        <Text style={[styles.availText, {color:'#428BF9'}, {fontSize:16}, {marginTop:10}]}> OR </Text>
        <Text style={[styles.availText, {color:'#428BF9'}, {fontSize:16}]}>Customize time for each day </Text>
        
        <TouchableOpacity style={styles.buttonAvail}
            onPress={() => {this.props.navigation.navigate("MenuScreen")}}>
              <Text style={styles.start}> Save </Text>
          </TouchableOpacity>
      
        
      </ScrollView>
    ); 
  }
};
