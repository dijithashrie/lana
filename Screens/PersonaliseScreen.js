import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, LinearGradient,Dimensions } from 'react-native';
const width = Dimensions.get('window').width

export default class PersonaliseScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <ScrollView>
        <Text style={styles.textstyle} > First, lets personalise </Text>
        <Text style={styles.textstyle}> your space </Text>
        <Text style={styles.tstyle}> Choose the challenge you'd like to work on. You can as many as you like :) </Text>
        <View style={{flex: 1, flexDirection: 'row', marginTop:50,
        justifyContent: 'space-around',}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FAD9D2'}]} onPress={() => {}}>
          <Text style={styles.inside}> Anxity </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#E2FFCC'}]} onPress={() => {}}>
        <Text style={styles.inside}> Work Stress </Text>
        </TouchableOpacity>

        </View>
        

        <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around',}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#E7F0FF'}]} onPress={() => {}}>
          <Text style={styles.inside}> Motivation </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FDDFCE'}]} onPress={() => {}}>
        <Text style={styles.inside}> Low Energy </Text>
        </TouchableOpacity>

        </View>

        <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around',}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#C0E9FF'}]} onPress={() => {}}>
          <Text style={styles.inside}> Self Esteem </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#C1DAFF'}]} onPress={() => {}}>
        <Text style={styles.inside}> Depression </Text>
        </TouchableOpacity>

        </View>

        <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around',}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#E2FFCC'}]} onPress={() => {}}>
          <Text style={styles.inside}> Loneliness </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FCBBC1'}]} onPress={() => {}}>
        <Text style={styles.inside}> Health Issues </Text>
        </TouchableOpacity>

        </View>

        <View style={{flex: 1, flexDirection: 'row', marginTop:10,
        justifyContent: 'space-around',}} >
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#FDECCE'}]} onPress={() => {}}>
          <Text style={styles.inside}> Trauma </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rectangle, {backgroundColor: '#8DD1D4'}]} onPress={() => {}}>
        <Text style={styles.inside}> Loss </Text>
        </TouchableOpacity>

        </View>
        <View >
          <TouchableOpacity style={[{ backgroundColor: "#3B5998"} , {borderRadius: 15}, {paddingVertical: 10,}, {alignSelf:'center'}, {marginBottom:100}, {marginTop:20}]}
            onPress={() => {this.props.navigation.navigate("Preffer")}}>
              <Text style={styles.start}>   Maybe Later   </Text>
          </TouchableOpacity>
        </View>

        
      </ScrollView>



     
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E5E5E5',
  },
  textstyle:{
    fontStyle:'normal',
    fontSize:28,
    textAlign:'center'
  },
  tstyle:{
    fontStyle:'normal',
    fontWeight:'normal',
    textAlign:'center',
    fontSize:16,
    color:'#428BF9',
    marginTop:10
  },
  rectangle: {
    height: 75,
    width: 157,
    borderRadius:14,
    
    
    
  },
  inside:{
    fontStyle:'normal',
    fontWeight:'normal',
    textAlign:'center',
    marginTop:20,
   
    fontSize:22,
    color:'#000000',
   

  },

  button: {
    backgroundColor: "#FFFFFF",
    padding: 20,
    marginTop:30,
    width:200,
    height:60,
    borderRadius:27,
    justifyContent: 'center',
    alignItems: 'center',
    left:80
   
    
  
    //TODO

  },
  start:{
    fontSize:24,
    fontWeight:'normal',
    color:'#000000',
    textAlign:'center',
    justifyContent:'center'


  },
  


}); 