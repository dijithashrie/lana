import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


import Icon from 'react-native-vector-icons/Ionicons';  

import SignInScreen from './Screens/SignInScreen';
import FacebookLogin from './Screens/FacebookLogin';
import HomeScreen  from './Screens/HomeScreen';
import  PersonaliseScreen from './Screens/PersonaliseScreen';
import PrefferScreen from './Screens/PrefferScreen';
import AvailabilityScreen from './Screens/AvailabilityScreen'
import MenuScreen from './Screens/MenuScreen'
import AppointmentsScreen from './Screens/AppointmentsScreen';
import MyExcerscisesScreen from './Screens/MyExcerscisesScreen';
import MoodTrackerScreen from './Screens/MoodTrackerScreen';
import MyJournalScreen from './Screens/MyJournalScreen';
import SettingsScreen from './Screens/SettingsScreen';
import ChatScreen from './Screens/ChatScreen';
import NewJournalScreen from './Screens/NewJournalScreen';
import CalenderScreen from './Screens/CalenderScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialBottomTabNavigator();



function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="Signin" component={SignInScreen}     />
        <Stack.Screen name="Facebook" component={FacebookLogin} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Personalize" component={PersonaliseScreen} />
        <Stack.Screen name="Preffer" component={PrefferScreen} />
        <Stack.Screen name="Avil" component={AvailabilityScreen}/>
        <Stack.Screen name="MenuScreen" component={MyDrawer} options={{headerShown:false}}
       
                             
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


const MyDrawer = props => (
  <Drawer.Navigator
  screenOptions={{
    headerShown: false
  }}
    >
        <Drawer.Screen name="MenuScreen" component={MenuScreen} options={{headerShown:false}}/>
        <Drawer.Screen name="Chat" component={ChatScreen} />
        <Drawer.Screen name="Journal" component={MyTabs} />

        <Drawer.Screen name="Appointments" component={AppointmentsScreen} />
        <Drawer.Screen name="MyExcercise" component={MyExcerscisesScreen} />
        <Drawer.Screen name="MoodTracker" component={MoodTrackerScreen} />
        <Drawer.Screen name="Settings" component={SettingsScreen} />
  </Drawer.Navigator>
);

function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="MyJournal" component={MyJournalScreen} 
      options={{
        tabBarLabel: 'MyJournal',
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="home" color={color} size={26} />
        ),
      }}/>
    <Tab.Screen name="NewJournal " component={NewJournalScreen} 
     options={{
      tabBarLabel: 'NewJournal',
      tabBarIcon: ({ color }) => (
        <MaterialCommunityIcons name="calendar" color={color} size={26} />
      ),
    }}/>


    <Tab.Screen name="Calender" component={CalenderScreen}
     options={{
      tabBarLabel: 'Calender',
      tabBarIcon: ({ color }) => (
        <MaterialCommunityIcons name="calendar" color={color} size={26} />
      ),
    }}/> 

    </Tab.Navigator>
  );
}


export default App;


// const Stack = createStackNavigator();
// const Drawer = createDrawerNavigator();



// // function MyDrawer() {
// //   return (
// //     <Drawer.Navigator  initialRouteName="MenuScreen" >

// //       <Drawer.Screen name="MenuScreen" component={MenuScreen} />
// //       <Drawer.Screen name="Appointments" component={AppointmentsScreen} />
// //       <Drawer.Screen name="MyExcercise" component={MyExcerscisesScreen} />
// //       <Drawer.Screen name="MoodTracker" component={MoodTrackerScreen} />
// //       <Drawer.Screen name="MyJournal" component={MyJournalScreen} />
// //       <Drawer.Screen name="Settings" component={SettingsScreen} />


// //     </Drawer.Navigator>
// //   );
// // }

// const MyDrawer = props => (
//   <Drawer.Navigator
//     drawerStyle={{
//       backgroundColor: 'violet',
//       width: 240,
//     }}>
//     <Drawer.Screen name="MenuScreen" component={MenuScreen} />
//       <Drawer.Screen name="Appointments" component={AppointmentsScreen} />
//       <Drawer.Screen name="MyExcercise" component={MyExcerscisesScreen} />
//       <Drawer.Screen name="MoodTracker" component={MoodTrackerScreen} />
//       <Drawer.Screen name="MyJournal" component={MyJournalScreen} />
//       <Drawer.Screen name="Settings" component={SettingsScreen} />
//   </Drawer.Navigator>
// );


// function App({Navigation}) {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator 
      
      
//       >
//         <Stack.Screen name="Signin" component={SignInScreen} 
//         />
//         <Stack.Screen name="Facebook" component={FacebookLogin} />
//         <Stack.Screen name="Home" component={HomeScreen} />
//         <Stack.Screen name="Personalize" component={PersonaliseScreen} />
//         <Stack.Screen name="Preffer" component={PrefferScreen} />
//         <Stack.Screen name="Avil" component={AvailabilityScreen}/>
//         <Stack.Screen name="MenuScreen" component={MyDrawer}
//         //error
//         options={{
//           headerLeft: () => <Icon
//               name="ios-menu"
//               size={25}
//               color="#D4AF37"
//               onPress={() => props.navigation.navigate('DrawerOpen')}            /> }}
//             />
       
      
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }

// export default App;
